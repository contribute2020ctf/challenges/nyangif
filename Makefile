all: gif

jar:
	$(MAKE) -C HelloFlag jar
gif: jar
	cat nyan.gif HelloFlag/target/HelloFlag-1.0-SNAPSHOT.jar > out.gif


clean:
	$(MAKE) -C HelloFlag clean
	rm out.gif
